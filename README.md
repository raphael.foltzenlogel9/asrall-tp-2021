# Raphael Foltzenlogel

Statement of work ([framagit.org](https://framagit.org/Katyucha/asrall-tp-2021/-/blob/main/TP.md))



## Requirements

```
ansible-galaxy collection install community.general
```


## Firewall rules


### Setting default policies:
- `iptables -P INPUT DROP`
- `iptables -P FORWARD DROP`
- `iptables -P OUTPUT DROP`


### Exceptions to default policy

- Allow connection already open to receive traffic
  - `iptables -A INPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT`
  - `iptables -A OUTPUT -m conntrack --ctstate ESTABLISHED -j ACCEPT`


- SSH
  - `iptables -A INPUT -p tcp -i eth0 --dport ssh -j ACCEPT`
  - `iptables -A INPUT -p tcp -i eth1 -s 192.168.33.1 -d 192.168.33.17 --dport ssh -j ACCEPT`


- Web server
  - `iptables -A INPUT -p tcp -i eth1 --dport 80 -j ACCEPT`
  - `iptables -A INPUT -p tcp -i eth1 --dport 81 -j ACCEPT`
  - `iptables -A INPUT -p tcp -i eth1 --dport 82 -j ACCEPT`
  - `iptables -A INPUT -p tcp -i eth1 --dport 83 -j ACCEPT`


- Allow install/update with the APT package manager  
  - `iptables -A OUTPUT -p tcp -o eth0 --dport 53 -j ACCEPT`
  - `iptables -A OUTPUT -p tcp -o eth0 -m multiport --dport 80,443 -j ACCEPT`


- Ping
  - not essential


- Web server Nginx
  - `iptables -A INPUT -p tcp -i eth1 --dport 9000 -j ACCEPT`



## Source Documents
- handlers ([docs.ansible.com](https://docs.ansible.com/ansible/latest/user_guide/playbooks_handlers.html))
- state: present ([docs.ansible.com](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/yum_module.html#parameter-state))
- iptables ([docs.ansible.com](https://docs.ansible.com/ansible/latest/collections/ansible/builtin/iptables_module.html))
- iptables_state_module ([docs.ansible.com](https://docs.ansible.com/ansible/latest/collections/community/general/iptables_state_module.html))
